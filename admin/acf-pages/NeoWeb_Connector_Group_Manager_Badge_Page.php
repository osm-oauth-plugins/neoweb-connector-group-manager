<?php


class NeoWeb_Connector_Group_Manager_Badge_Page {

	private $pageID;
	private $plugin_data;

	/**
	 * @param $key
	 *
		 * @return string
		 */
	public function get_plugin_data($key): string {
		return $this->plugin_data[$key];
	}

	/**
	 * __constructor.
	 */
	public function __construct()
	{

		$this->plugin_data = get_option('neoweb-connector-group-manager');
		$this->pageID = $this->get_plugin_data('pluginSlug') . '_badge_progress_page';
	}

	public function registerPage() {

		if( function_exists('acf_add_options_page') ):

			acf_add_options_page(array(
				'page_title' => 'NeoWeb Connector - Badge Progress Settings',
				'menu_title' => 'Badge Progress',
				'menu_slug' => $this->pageID,
				'capability' => 'manage_options',
				'position' => '',
				'parent_slug' => $this->get_plugin_data('pluginSlug') . '_parent',
				'icon_url' => '',
				'redirect' => true,
				'post_id' => 'options',
				'autoload' => false,
				'update_button' => 'Update',
				'updated_message' => 'Options Updated',
			));

		endif;

	}

	public function registerLogo() {
		$img_folder_path = plugin_dir_url( dirname(__FILE__) );
		if( function_exists('acf_add_local_field_group') ):
			acf_add_local_field_group(array(
				'key' => 'group_logo' . $this->pageID,
				'title' => 'plugin_logo',
				'fields' => array(
				),
				'location' => array(
					array(
						array(
							'param' => 'options_page',
							'operator' => '==',
							'value' => $this->pageID,
						),
					),
				),
				'menu_order' => -1,
				'position' => 'acf_after_title',
				'style' => 'seamless',
				'label_placement' => 'top',
				'instruction_placement' => 'field',
				'hide_on_screen' => '',
				'active' => true,
				'description' => '',
			));

			acf_add_local_field(array(
				'key' => 'field_' . 'logo_' . $this->pageID,
				'label' => '',
				'name' => 'logo_' . $this->pageID,
				'type' => 'message',
				'message' => '<div class="logoWrapper">
        <img src="' . $img_folder_path . '/images/logo.png"></div>',
				'parent' => 'group_logo' . $this->pageID,
			));
		endif;
	}

	public function registerFields() {
		if( function_exists('acf_add_local_field_group') ):

			acf_add_local_field_group(array(
				'key' => 'group_' . 'badge_progress',
				'title' => 'Badge Progress Options',
				'fields' => array(
					array(
						'key' => 'field_6018293b204bc',
						'label' => '',
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => '<p style="text-align: center;" class="neowebNotice">
										For best results, term dates should ideally run back to back in OSM, with the end date of a term set to the day before the start of the next term. Gaps in term dates will result in no data found in this section.
								</p>',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
				),
				'location' => array(
					array(
						array(
							'param' => 'options_page',
							'operator' => '==',
							'value' => $this->pageID,
						),
					),
				),
				'menu_order' => 0,
				'position' => 'normal',
				'style' => 'seamless',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => true,
				'description' => '',
			));

			acf_add_local_field_group(array(
					'key' => 'group_' . 'badge_progress_options',
					'title' => 'Badge Progress Options',
					'fields' => array(
						array(
							'key' => 'young_person_name_display',
							'label' => 'Young Person Name Display',
							'name' => 'young_person_name_display',
							'type' => 'select',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'choices' => array(
								'id_only' => 'OSM ID',
								'firstName' => 'First name only',
								'firstName+' => 'First name + First 2 characters from surname',
								'lastName' => 'Surname only',
								'lastname+' => 'Surname + First 2 characters from firstname',
								'fullname' => 'Surname + Firstname',
								'fullname2' => 'Firstname + Surname',
							),
							'default_value' => 'firstName+',
							'allow_null' => 0,
							'multiple' => 0,
							'ui' => 0,
							'return_format' => 'value',
							'ajax' => 0,
							'placeholder' => '',
						),
						array(
							'key' => 'show_lodgepackpatrol_badge',
							'label' => 'Show Lodge/Pack/Patrol Badge?',
							'name' => 'show_lodgepackpatrol_badge',
							'type' => 'true_false',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'message' => '',
							'default_value' => 0,
							'ui' => 0,
							'ui_on_text' => '',
							'ui_off_text' => '',
						),
						array(
							'key' => 'show_lodgepackpatrol_rank',
							'label' => 'Show Lodge/Pack/Patrol Rank?',
							'name' => 'show_lodgepackpatrol_rank',
							'type' => 'true_false',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'message' => '',
							'default_value' => 0,
							'ui' => 0,
							'ui_on_text' => '',
							'ui_off_text' => '',
						),
					),
					'location' => array(
						array(
							array(
								'param' => 'options_page',
								'operator' => '==',
								'value' => $this->pageID,
							),
						),
					),
					'menu_order' => 10,
					'position' => 'normal',
					'style' => 'default',
					'label_placement' => 'left',
					'instruction_placement' => 'label',
					'hide_on_screen' => '',
					'active' => true,
					'description' => '',
					'acfe_display_title' => '',
					'acfe_autosync' => '',
					'acfe_form' => 0,
					'acfe_meta' => '',
					'acfe_note' => '',
				));

		endif;
	}

	public function registerShortCodes ($sectionName, $sectionID, $sectionType) {
		if( function_exists('acf_add_local_field_group') ):
			acf_add_local_field_group(array(
			'key' => 'group_bargeProgress_group' . $sectionID,
			'title' => 'Available [Short-Codes] for ' . $sectionName,
			'fields' => array(
				array(
					'key' => 'badge_progress_' . $sectionID,
					'label' => 'Available shortcodes for this section',
					'name' => 'badge_progress_' . $sectionID,
					'type' => 'message',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => '',
					'new_lines' => 'wpautop',
					'esc_html' => 0,
				),
				array(
					'key' => 'badge_progress_all_' . $sectionID,
					"label" => "Badge Progress (All Badges)",
					'name' => 'badge_progress_all_' . $sectionID,
					'type' => 'text',
					'wrapper' => array(
						'class' => 'shortCodeCopy',
					),
					'readonly'=> 1,
					'default_value' => '[OSM_badge sectionid="' . $sectionID . '" section="'. $sectionType . '" badgetype="all" show-progress="true" show-awarded="true"]',
				),
				array(
					'key' => 'badge_progress_challenge_' . $sectionID,
					"label" => "Badge Progress (Challenge Badges)",
					'name' => 'badge_progress_challenge_' . $sectionID,
					'type' => 'text',
					'wrapper' => array(
						'class' => 'shortCodeCopy',
					),
					'readonly'=> 1,
					'default_value' => '[OSM_badge sectionid="' . $sectionID . '" section="'. $sectionType . '" badgetype="challenge" show-progress="true" show-awarded="true"]',
				),
				array(
					'key' => 'badge_progress_activity_' . $sectionID,
					"label" => "Badge Progress (Activity Badges)",
					'name' => 'badge_progress_activity_' . $sectionID,
					'type' => 'text',
					'wrapper' => array(
						'class' => 'shortCodeCopy',
					),
					'readonly'=> 1,
					'default_value' => '[OSM_badge sectionid="' . $sectionID . '" section="'. $sectionType . '" badgetype="activity" show-progress="true" show-awarded="true"]',
				),
				array(
					'key' => 'badge_progress_staged_' . $sectionID,
					"label" => "Badge Progress (Staged Badges)",
					'name' => 'badge_progress_staged_' . $sectionID,
					'type' => 'text',
					'wrapper' => array(
						'class' => 'shortCodeCopy',
					),
					'readonly'=> 1,
					'default_value' => '[OSM_badge sectionid="' . $sectionID . '" section="'. $sectionType . '" badgetype="staged" show-progress="true" show-awarded="true"]',
				)
			),
			'location' => array(
				array(
					array(
						'param' => 'options_page',
						'operator' => '==',
						'value' => $this->pageID,
					),
				),
			),
			'menu_order' => 30,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'field',
			'hide_on_screen' => '',
			'active' => 1,
			'description' => '',
		));
		endif;
	}
}