<?php


class NeoWeb_Connector_Group_Manager_Events_Page {

	private string $pageID;
	private $plugin_data;

	/**
	 * @param $key
	 *
		 * @return string
		 */
	public function get_plugin_data($key): string {
		return $this->plugin_data[$key];
	}

	/**
	 * __constructor.
	 */
	public function __construct()
	{
		$this->plugin_data = get_option('neoweb-connector-group-manager');
		$this->pageID = $this->get_plugin_data('pluginSlug') . '_events_page';
	}

	public function registerPage() {
		if( function_exists('acf_add_options_page') ):

			acf_add_options_page(array(
				'page_title' => 'NeoWeb Connector - Event Settings',
				'menu_title' => 'Events',
				'menu_slug' => $this->pageID,
				'capability' => 'manage_options',
				'position' => '',
				'parent_slug' => $this->get_plugin_data('pluginSlug') . '_parent',
				'icon_url' => '',
				'redirect' => true,
				'post_id' => 'options',
				'autoload' => false,
				'update_button' => 'Update',
				'updated_message' => 'Options Updated',
			));

		endif;
	}

	public function registerLogo() {
		$img_folder_path = plugin_dir_url( dirname(__FILE__) );
		if( function_exists('acf_add_local_field_group') ):
			acf_add_local_field_group(array(
				'key' => 'group_logo' . $this->pageID,
				'title' => 'plugin_logo',
				'fields' => array(
				),
				'location' => array(
					array(
						array(
							'param' => 'options_page',
							'operator' => '==',
							'value' => $this->pageID,
						),
					),
				),
				'menu_order' => -1,
				'position' => 'acf_after_title',
				'style' => 'seamless',
				'label_placement' => 'top',
				'instruction_placement' => 'field',
				'hide_on_screen' => '',
				'active' => true,
				'description' => '',
			));

			acf_add_local_field(array(
				'key' => 'field_' . 'logo_' . $this->pageID,
				'label' => '',
				'name' => 'logo_' . $this->pageID,
				'type' => 'message',
				'message' => '<div class="logoWrapper">
        <img src="' . $img_folder_path . '/images/logo.png"></div>',
				'parent' => 'group_logo' . $this->pageID,
			));
		endif;
	}

	public function registerFields() {

		if( function_exists('acf_add_local_field_group') ):

			acf_add_local_field_group(array(
				'key' => 'group_' . 'event_options',
				'title' => 'Event Options',
				'fields' => array(
					array(
						'key' => 'field_event_options',
						'label' => '',
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => '<p style="text-align: center;" class="neowebNotice">
										For best results, term dates should ideally run back to back in OSM, with the end date of a term set to the day before the start of the next term. Gaps in term dates will result in no data found in this section.
								</p>',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
				),
				'location' => array(
					array(
						array(
							'param' => 'options_page',
							'operator' => '==',
							'value' => $this->pageID,
						),
					),
				),
				'menu_order' => 0,
				'position' => 'normal',
				'style' => 'seamless',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => true,
				'description' => '',
			));

			acf_add_local_field_group(array(
				'key' => 'group_' . 'event_options_fields',
				'title' => 'Event Options',
				'fields' => array(
				),
				'location' => array(
					array(
						array(
							'param' => 'options_page',
							'operator' => '==',
							'value' => $this->pageID,
						),
					),
				),
				'menu_order' => 10,
				'position' => 'normal',
				'style' => 'default',
				'label_placement' => 'left',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => true,
				'description' => '',
			));

		endif;

	}

	public function registerShortCodes ($sectionName, $sectionID) {
		if ( function_exists( 'acf_add_local_field_group' ) ):
			acf_add_local_field_group(array(
				'key' => 'group_events' . $sectionID,
				'title' => 'Available [Short-Codes] for ' . $sectionName,
				'fields' => array(
					array(
						'key' => 'events_short_codes' . $sectionID,
						'label' => 'Available shortcodes for this section',
						'name' => 'events_short_codes' . $sectionID,
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => '',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array(
						'key' => 'events_summery_short_code' . $sectionID,
						"label" => "Event Summery Widget",
						'name' => 'events_summery_short_code' . $sectionID,
						'type' => 'text',
						'wrapper' => array(
							'class' => 'shortCodeCopy',
						),
						'readonly'=> 1,
						'default_value' => '[OSM_Event_Summary sectionid="' . $sectionID .'"]',
					)
				),
				'location' => array(
					array(
						array(
							'param' => 'options_page',
							'operator' => '==',
							'value' => $this->pageID,
						),
					),
				),
				'menu_order' => 30,
				'position' => 'normal',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'field',
				'hide_on_screen' => '',
				'active' => 1,
				'description' => '',
			));
		endif;
	}

}