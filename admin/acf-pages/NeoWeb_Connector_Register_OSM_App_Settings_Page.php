<?php


class NeoWeb_Connector_Register_OSM_App_Settings_Page {

	private $pageID;
	private $plugin_data;

	/**
	 * @param $key
	 *
		 * @return string
		 */
	public function get_plugin_data($key): string {
		return $this->plugin_data[$key];
	}

	/**
	 *
	 */
	public function __construct() {
		$this->plugin_data = get_option('neoweb-connector-group-manager');
		$this->pageID = $this->get_plugin_data('productSlug') . '-app-settings';
	}

	public function registerGenericInstructions () {
		$img_folder_path = plugin_dir_url( dirname(__FILE__) );
		if( function_exists('acf_add_local_field_group') ):
			acf_add_local_field_group(array(
				'key' => 'group_' . $this->get_plugin_data('productSlug') . '_application_instructions',
				'title' => 'Creating a new application in OSM',
				'fields' => array(
					array(
						'key' => $this->get_plugin_data('productSlug') . '_install_instructions',
						'label' => '',
						'name' => $this->get_plugin_data('productSlug') . '_install_instructions',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => '<div class="wrap">
        <ol>
            <li>Navigate to <a href="https://www.onlinescoutmanager.co.uk/" target="_blank">OSM</a></li>
            <li>Login using your main login details</li>
            <li>
                <p>Click on "Settings" link</p>
                <img src="'. $img_folder_path . '/images/osm-settings.png">
            </li>
            <li>
                <p>Click on "My Account Details" link</p>
                <img src="' . $img_folder_path . '/images/osm-settings-my-account-details.png">
            </li>
            <li>
                <p>Click on "Developer Tools" link</p>
                <img src="' . $img_folder_path . '/images/osm-settings-developer-tools.png">
            </li>
            <li>
                <p>Click on the "Create Application" button</p>
                <img src="' . $img_folder_path . '/images/osm-settings-create-application.png">
            </li>
            <li>
                <p>IMPORTANT: Enter <strong>"NeoWeb_Connector_Statistics"</strong> as your application name, and click save</p>
                <img src="' . $img_folder_path . '/images/osm-settings-application-name.png">
            </li>
            <li>
                <p>Copy the OAuth Client ID and OAuth Secret values and paste the values above, and then click close</p>
                <img src="' . $img_folder_path . '/images/osm-settings-oAuth-settings.png" style="width: 100%;">
            </li>
            <li>
                <p>Click on "NeoWeb_Connector_Statistics"</p>
                <img src="' . $img_folder_path . '/images/osm-settings-application-settings.png">
            </li>
            <li>
                <p>IMPORTANT: Enter the following redirect url <strong>"' . get_admin_url() . 'admin.php?page=neoweb-connector-statistics-manager_settings_page"</strong>, and then click save</p>
                <img src="' . $img_folder_path . '/images/osm-settings-redirect-urls.png">
            </li>
            <li>
            	<p>Click the "Authenticate Plugin" button to the right.</p>
            	<p><strong>Please note:</strong> You will be redirected to OSM, where you will be asked to enter 
            	your username and password, please use the same details you used in step 2 above.</p>
            	<p>If you are not redirect to OSM it may be that you have completed this step before and your login is 
            	still cached, simply continue to the next step</p>
            </li>
            <li>
            	<p><strong>Very Important:</strong></p>
            	<p>Click the "Authenticate Plugin" button to the right again, once you have completed step 11.</p>
            	<p>If the authentication has been completed successfully you should be presented with the following 
            	message: OSM has been successfully authenticated.</p>
			</li>
        </ol></div>',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
				),
				'location' => array(
					array(
						array(
							'param' => 'options_page',
							'operator' => '==',
							'value' => $this->pageID,
						),
					),
				),
				'menu_order' => 0,
				'position' => 'acf_after_title',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => true,
				'description' => ''
			));
		endif;
	}

	public function registerApplicationFields () {
		if( function_exists('acf_add_local_field_group') ):

			acf_add_local_field_group(array(
				'key' => 'group_' . $this->get_plugin_data('pluginSlug') . '_application_fields',
				'title' => $this->get_plugin_data('pluginName') . ' - Application Settings',
				'fields' => array(
					array(
						'key' => $this->get_plugin_data('pluginSlug') . '_osm_oauth_client_id',
						'label' => 'OSM oAuth Client ID',
						'name' => $this->get_plugin_data('pluginSlug') . '_osm_oauth_client_id',
						'type' => 'text',
						'instructions' => '',
						'required' => 1,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => $this->get_plugin_data('pluginSlug') . '_osm_oauth_secret',
						'label' => 'OSM oAuth Secret',
						'name' => $this->get_plugin_data('pluginSlug') . '_osm_oauth_secret',
						'type' => 'text',
						'instructions' => '',
						'required' => 1,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => $this->get_plugin_data('pluginSlug') . '_osm_permissions_badge',
						'label' => 'Badges Records',
						'name' => $this->get_plugin_data('pluginSlug') . '_osm_permissions_badge',
						'type' => 'radio',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => 'disabled',
							'id' => '',
						),
						'choices' => array(
							'read' => 'Read Only',
							'write' => 'Read & Write',
							'none' => 'No Access',
						),
						'allow_null' => 0,
						'other_choice' => 0,
						'default_value' => 'read',
						'layout' => 'horizontal',
						'return_format' => 'value',
						'save_other_choice' => 0,
					),
					array(
						'key' => $this->get_plugin_data('pluginSlug') . '_osm_permissions_event',
						'label' => 'Events',
						'name' => $this->get_plugin_data('pluginSlug') . '_osm_permissions_event',
						'type' => 'radio',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => 'disabled',
							'id' => '',
						),
						'choices' => array(
							'read' => 'Read Only',
							'write' => 'Read & Write',
							'none' => 'No Access',
						),
						'allow_null' => 0,
						'other_choice' => 0,
						'default_value' => 'read',
						'layout' => 'horizontal',
						'return_format' => 'value',
						'save_other_choice' => 0,
					),
					array(
						'key' => $this->get_plugin_data('pluginSlug') . '_osm_permissions_member',
						'label' => 'Members',
						'name' => $this->get_plugin_data('pluginSlug') . '_osm_permissions_member',
						'type' => 'radio',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => 'disabled',
							'id' => '',
						),
						'choices' => array(
							'read' => 'Read Only',
							'write' => 'Read & Write',
							'none' => 'No Access',
						),
						'allow_null' => 0,
						'other_choice' => 0,
						'default_value' => 'read',
						'layout' => 'horizontal',
						'return_format' => 'value',
						'save_other_choice' => 0,
					),
					array(
						'key' => $this->get_plugin_data('pluginSlug') . '_osm_permissions_programme',
						'label' => 'Programme',
						'name' => $this->get_plugin_data('pluginSlug') . '_osm_permissions_programme',
						'type' => 'radio',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => 'disabled',
							'id' => '',
						),
						'choices' => array(
							'read' => 'Read Only',
							'write' => 'Read & Write',
							'none' => 'No Access',
						),
						'allow_null' => 0,
						'other_choice' => 0,
						'default_value' => 'read',
						'layout' => 'horizontal',
						'return_format' => 'value',
						'save_other_choice' => 0,
					),
					array(
						'key' => $this->get_plugin_data('pluginSlug') .'_maxRateLimit',
						'label' => 'Maximum API calls per cycle: ',
						'name' => $this->get_plugin_data('pluginSlug') .'_maxRateLimit',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'readonly' => 1,
						'conditional_logic' => 0,
						'message' => '',
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => $this->get_plugin_data('pluginSlug') .'_currentRateLimit',
						'label' => 'Remaining API calls this cycle: ',
						'name' => $this->get_plugin_data('pluginSlug') . '_currentRateLimit',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'readonly' => 1,
						'conditional_logic' => 0,
						'message' => '',
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => $this->get_plugin_data('pluginSlug') .'_rateLimitRefresh',
						'label' => 'Next cycle refresh: ',
						'name' => $this->get_plugin_data('pluginSlug') .'_rateLimitRefresh',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'readonly' => 1,
						'conditional_logic' => 0,
						'message' => '',
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => $this->get_plugin_data('pluginSlug') .'_accessToken',
						'label' => '',
						'name' => $this->get_plugin_data('pluginSlug') .'_accessToken',
						'type' => 'textarea',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => '',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array(
						'key' => $this->get_plugin_data('pluginSlug') . '_test_api',
						'label' => '',
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => '<button type="button" class="button-secondary" id="'. str_replace("-", "_", $this->get_plugin_data('pluginSlug')) .'triggerAPITest">Test OSM API</button>',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
				),
				'location' => array(
					array(
						array(
							'param' => 'options_page',
							'operator' => '==',
							'value' => $this->pageID,
						),
					),
				),
				'menu_order' => 10,
				'position' => 'normal',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => true,
				'description' => ''
			));

		endif;
	}
}