<?php


class NeoWeb_Connector_Group_Manager_Register_Shortcodes {


	private NeoWeb_Connector_Group_Manager_Auth_Caller $oAuthCaller;
	private NeoWeb_Connector_Loggers $logger;

	private $plugin_data;

	/**
	 * @param $key
	 *
		 * @return string
		 */
	public function get_plugin_data($key): string {
		return $this->plugin_data[$key];
	}

	/**
	 * __contructor
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		$this->plugin_data = get_option('neoweb-connector-group-manager');
		$this->oAuthCaller = new NeoWeb_Connector_Group_Manager_Auth_Caller();
	}

	public function create_points_report($attr) {
		$sectionID = $attr['sectionid'];
		$currentTermID = $this->oAuthCaller->getCurrentTermID($sectionID);
		$html = "";
		if ($currentTermID != "") {
			$include_members = get_field('show_young_people', 'option');
			$include_points = get_field('show_points', 'option');
			$pre_points_message = get_field('text_to_show_before_points', 'option');
			$post_points_message = get_field('text_to_show_after_points', 'option');
			$include_rank = get_field('show_young_persons_rank', 'option');

			$url = NeoWeb_Connector_Group_Manager_OSM_Endpoints::getPatrolPoints;
			$formattedURL = (new NeoWeb_Connector_Group_Manager_OSM_Endpoints)->formatEndPoint($url, $sectionID, $currentTermID);

			$transientID = 'section_points_data_' . $sectionID;
			$patrolData = json_decode($this->oAuthCaller->osmAPICaller($transientID, $formattedURL, 12, array()), true);

			ob_start();
			$html = ob_get_clean();
			$html .= '<div id="accordionPoints" role="tablist">';
			foreach ($patrolData as $key=>$sub_section) {
				if (array_key_exists('patrolid', $sub_section) && $sub_section['patrolid'] > 0) { //Normal state active
					$sectionName = $sub_section['name'];
					$sectionPoints = $sub_section['points'];
					$sectionMembers = $sub_section['members'];
					ob_start();
					include(OSM_GROUP_PLUGIN_PATH . 'public/html-templates/section-points.php');
					$html .= ob_get_clean();
				} else if (array_key_exists('status', $sub_section) && $sub_section['status'] == false) { //Error State active - sample maintenance mode
					$errorMsg = $sub_section['error']['message'];
					ob_start();
					include(OSM_GROUP_PLUGIN_PATH . 'public/html-templates/error-message.php');
					$html .= ob_get_clean();
				}
			}
			$html .= "</div>";
		} else {
			ob_start();
			include(OSM_GROUP_PLUGIN_PATH . 'public/html-templates/error-no-active-term.php');
			$html .= ob_get_clean();
		}

		$html .= '<p style="font-size: 12px; margin-top: 10px;" class="text-right"><small >Powered by: <a href="http://www.scoutsuk.org">NeoWeb Connector</a></small></p>';

		return $html;

	}

	public function fetch_osm_event_summary($attr) {
		$sectionID = $attr['sectionid'];
		$currentTermID = $this->oAuthCaller->getCurrentTermID($sectionID);
		$html = "";
		if ($currentTermID != "") {
			$url = NeoWeb_Connector_Group_Manager_OSM_Endpoints::getEventSummary;
			$formattedURL = (new NeoWeb_Connector_Group_Manager_OSM_Endpoints)->formatEndPoint($url, $sectionID, $currentTermID);

			$transientID = 'programSummary_data_' . $sectionID;
			$eventSummaryData = json_decode($this->oAuthCaller->osmAPICaller($transientID, $formattedURL, 12, array()), true);

			if (isset($eventSummaryData['events'])) {
				ob_start();
				$html = ob_get_clean();

				$html .= '<ul class="list-group eventSummary">';
				foreach ($eventSummaryData['events'] as $eventID) {
					$eventid = $eventID['eventid'];
					$eventdate = $eventID['date'];
					$title = $eventID['name'];
					ob_start();
					include( OSM_GROUP_PLUGIN_PATH . 'public/html-templates/event-summary.php' );
					$html .= ob_get_clean();
				}
				$html .= '</ul>';
			} else {
				$html = '<div class="alert alert-info" role="alert">
                    There is no events available.
                </div>';
			}

		} else {
			ob_start();
			include( OSM_GROUP_PLUGIN_PATH . 'public/html-templates/error-no-active-term.php');
			$html .= ob_get_clean();
		}

		$html .= '<p style="font-size: 12px; margin-top: 10px;" class="text-right"><small >Powered by: <a href="http://www.scoutsuk.org">NeoWeb Connector</a></small></p>';

		return $html;
	}

	public function fetch_osm_program_summary($attr) {
		$sectionID = $attr['sectionid'];
		$currentTermID = $this->oAuthCaller->getCurrentTermID($sectionID);
		$html = "";
		if ($currentTermID != "") {
			$url = NeoWeb_Connector_Group_Manager_OSM_Endpoints::getProgramSummary;
			$formattedURL = (new NeoWeb_Connector_Group_Manager_OSM_Endpoints)->formatEndPoint($url, $sectionID, $currentTermID);

			$transientID = 'programSummary_data_' . $sectionID;
			$programSummaryData = json_decode($this->oAuthCaller->osmAPICaller($transientID, $formattedURL, 12, array()), true);

			if (isset($programSummaryData['programme'])) {
				if (count($programSummaryData['programme'])) {
					ob_start();
					$html = ob_get_clean();

					$html .= '<ul class="meetingSummary list-group">';
					foreach ($programSummaryData['programme'] as $night) {
						$eveningid = $night['eveningid'];
						$meetingdate = $night['meetingdate'];
						$title = $night['title'];
						ob_start();
						include( OSM_GROUP_PLUGIN_PATH . 'public/html-templates/meeting-summary.php' );
						$html .= ob_get_clean();
					}
					$html .= '</ul>';
				} else {
					$html = '<div class="alert alert-info" role="alert">
                    There is no meetings available.
                </div>';
				}
			} else {
				$html = '<div class="alert alert-info" role="alert">
                    There is no meetings available.
                </div>';
			}

		} else {
			ob_start();
			include(OSM_GROUP_PLUGIN_PATH . 'public/html-templates/error-no-active-term.php');
			$html .= ob_get_clean();
		}
		$html .= '<p style="font-size: 12px; margin-top: 10px;" class="text-right"><small >Powered by: <a href="http://www.scoutsuk.org">NeoWeb Connector</a></small></p>';

		return $html;
	}

	public function fetch_osm_program($attr) {
		$sectionID = $attr['sectionid'];
		$currentTermID = $this->oAuthCaller->getCurrentTermID($sectionID);
		$html = "";
		if ($currentTermID != "") {
			$futureOnly = get_field('show_future_dates_only', 'option');
			//$allowCalDownload = get_field('show_calendar_download', 'option');
			$showParentHelperCount = get_field('show_number_of_parent_helpers_required', 'option');
			$showParentHelpNotes = get_field('show_notes_for_parent_helpers', 'option');
			$showParentNotes = get_field('show_notes_for_parents', 'option');
			$showGameNotes = get_field('show_game_notes', 'option');
			$showPreNotes = get_field('show_pre_meeting_notes', 'option');
			$showPostNotes = get_field('show_post_meeting_notes', 'option');
			$showLeaderNotes = get_field('show_leader_notes', 'option');
			$showBadges = get_field('show_linked_badges', 'option');

			$url = NeoWeb_Connector_Group_Manager_OSM_Endpoints::getProgramSummary;
			$formattedURL = (new NeoWeb_Connector_Group_Manager_OSM_Endpoints)->formatEndPoint($url, $sectionID, $currentTermID);

			$transientID = 'programSummary_data_' . $sectionID;
			$programSummaryData = json_decode($this->oAuthCaller->osmAPICaller($transientID, $formattedURL, 12, array()), true);

			$html = "";
			ob_start();
			$html = ob_get_clean();

			$html .= '<div id="accordionMeeting" role="tablist">';
			if ( ! empty($programSummaryData['items']) ) { //Normal state active
				foreach ($programSummaryData['items'] as $night) {
					if ($futureOnly == 1) {
						if (strtotime($night["meetingdate"]) >= strtotime("now")) {
							ob_start();
							include(OSM_GROUP_PLUGIN_PATH . 'public/html-templates/program-overview.php');
							$html .= ob_get_clean();
						}
					} else {
						ob_start();
						include(OSM_GROUP_PLUGIN_PATH . 'public/html-templates/program-overview.php');
						$html .= ob_get_clean();
					}
				}
			} else if ( empty($programSummaryData['items']) ) {
				$html .= '<div class="alert alert-info" role="alert">
                    There is no further meetings available.
                </div>';
			} else {
				$errorMsg = $programSummaryData['error']['message'];
				ob_start();
				include(OSM_GROUP_PLUGIN_PATH . 'public/html-templates/error-message.php');
				$html .= ob_get_clean();
			}
			$html .= '</div>';
		} else {
			ob_start();
			include(OSM_GROUP_PLUGIN_PATH . 'public/html-templates/error-no-active-term.php');
			$html .= ob_get_clean();
		}

		$html .= '<p style="font-size: 12px; margin-top: 10px;" class="text-right"><small >Powered by: <a href="http://www.scoutsuk.org">NeoWeb Connector</a></small></p>';

		return $html;
	}

	public function fetch_osm_badge_data($attr) {

		$sectionType = $attr['section'];
		$sectionID = $attr['sectionid'];
		$currentTermID = $this->oAuthCaller->getCurrentTermID($sectionID);
		$html = "";
		if ($currentTermID != "") {
			$url = NeoWeb_Connector_Group_Manager_OSM_Endpoints::getBadgesByPerson;
			$formattedURL = (new NeoWeb_Connector_Group_Manager_OSM_Endpoints)->formatEndPoint($url, $sectionID, $currentTermID, $sectionType);

			$transientID = 'all_badges_data_' . $sectionID;
			$badgeProgressData = json_decode($this->oAuthCaller->osmAPICaller($transientID, $formattedURL, 12, array()), true);

			$badgeRecord = array();

			$counter = 0;
			if ( ! empty($badgeProgressData['data']) ) { //Normal state active
				foreach ($badgeProgressData['data'] as $yp) {
					$nameDisplay = get_field('young_person_name_display', 'option');

					if ($nameDisplay == "id_only") {
						$badgeRecord[$counter]["name"] = $yp['scout_id'];
					} else if ($nameDisplay == "firstName") {
						$badgeRecord[$counter]["name"] = $yp['firstname'];
					} else if ($nameDisplay == "firstName+") {
						$badgeRecord[$counter]["name"] = $yp['firstname'] . ' ' . substr($yp['lastname'], 0, 2);
					} else if ($nameDisplay == "lastName") {
						$badgeRecord[$counter]["name"] = $yp['lastname'];
					} else if ($nameDisplay == "lastName+") {
						$badgeRecord[$counter]["name"] = $yp['lastname'] . ' ' . substr($yp['firstname'], 0, 1);
					} else if ($nameDisplay == "fullname") {
						$badgeRecord[$counter]["name"] = $yp['lastname'].' '.$yp['firstname'];
					} else {
						$badgeRecord[$counter]["name"] = $yp['firstname'] . ' ' . substr($yp['lastname'], 0, 1);
					}

					$badgeRecord[$counter]["section"] = $sectionType;
					$badgeRecord[$counter]["lodge"] = $yp['patrol'];
					$badgeRecord[$counter]["lodge_level"] = $yp['patrol_role_level_label'];
					$badgeRecord[$counter]["scoutID"] = $yp['scout_id'];
					$badgeRecord[$counter]["age"] = $yp['age'];
					$badgeRecord[$counter]["badgeRecords"] = $yp['badges'];
					$counter++;
				}

				$badgetype = $attr['badgetype'];
				$showProgress = $attr['show-progress'];
				$showAwarded = $attr['show-awarded'];
				$show_lodge_badge = get_field('show_lodgepackpatrol_badge', 'option');
				$show_rank_badge = get_field('show_lodgepackpatrol_rank', 'option');

				ob_start();
				$html .= ob_get_clean();

				$html .= '<div id="badgeListAccordion" role="tablist">';
				foreach ($badgeRecord as $yp) {
					$name = $yp['name'];
					$lodge = $yp['lodge'];
					$lodge_level = $yp['lodge_level'];
					$scoutID = $yp['scoutID'];
					$badgeRecords = $yp['badgeRecords'];
					ob_start();
					include(OSM_GROUP_PLUGIN_PATH . 'public/html-templates/badge-progress-list.php');
					$html .= ob_get_clean();

				}
				$html .= '</div>';
			} else {
				$errorMsg = $badgeProgressData['error']['message'];
				ob_start();
				include(OSM_GROUP_PLUGIN_PATH . 'public/html-templates/error-message.php');
				$html .= ob_get_clean();
			}

		} else {
			ob_start();
			include(OSM_GROUP_PLUGIN_PATH . 'public/html-templates/error-no-active-term.php');
			$html .= ob_get_clean();
		}

		$html .= '<p style="font-size: 12px; margin-top: 10px;" class="text-right"><small >Powered by: <a href="http://www.scoutsuk.org">NeoWeb Connector</a></small></p>';

		return $html;
	}

	private function fetch_osm_program_data( $sectionID, $currentTermID, $eveningID ) {

		$plugin_folder_path = plugin_dir_url( __FILE__ );

		$domain = 'https://www.onlinescoutmanager.co.uk';

		$url = NeoWeb_Connector_Group_Manager_OSM_Endpoints::getProgramData;
		$formattedURL = (new NeoWeb_Connector_Group_Manager_OSM_Endpoints)->formatEndPoint($url, $sectionID, $currentTermID, $eveningID);

		$transientID = 'program_data_' . $sectionID;
		$programData = json_decode($this->oAuthCaller->osmAPICaller($transientID, $formattedURL, 12, array()), true);

		$innerHTML = "";
		$showFooter = false;
		foreach ($programData['badgelinks'] as $key=>$linkedBadges) {
			if (count($linkedBadges) > 0 ) {
				foreach ($linkedBadges as $linkedBadge) {
					$badgeID = $linkedBadge['badge_id'] . "_0";
					$badgeLabel = $linkedBadge['label'];
					$pictureUrl = $linkedBadge['picture'];
					ob_start();
					include( OSM_GROUP_PLUGIN_PATH . 'public/html-templates/print_badge_card.php' );
					$innerHTML .= ob_get_clean();
				}
				$showFooter = true;
			} else {
				$showFooter = false;
			}
		}

		if ($showFooter) {
			$html = '<div class="card-footer"><div class="row">';
			$html .= $innerHTML;
			$html .= '</div></div>';
		} else {
			$html="";
		}

		echo $html;

	}

}