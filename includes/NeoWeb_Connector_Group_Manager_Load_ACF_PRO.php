<?php

if( !class_exists('acf') ) {
	define( 'NEOWEB_GROUP_ACF_PATH', plugin_dir_path( dirname( __FILE__ ) ) . 'includes/libs/acf/' );
	define( 'NEOWEB_GROUP_ACF_URL', plugin_dir_url( dirname( __FILE__ ) ) . 'includes/libs/acf/' );

	// Include the ACF plugin.
	include_once( NEOWEB_GROUP_ACF_PATH . 'acf.php' );

	// Customize the url setting to fix incorrect asset URLs.
	add_filter( 'acf/settings/url', 'neoweb_group_settings_url' );
	function neoweb_group_settings_url( $url ): string {
		return NEOWEB_GROUP_ACF_URL;
	}

	// (Optional) Hide the ACF admin menu item.c
	add_filter( 'acf/settings/show_admin', 'neoweb_group_show_admin' );
	function neoweb_group_show_admin( $show_admin ): bool {
		return true;
	}
}