<div class="card">
	<div class="card-header" role="tab" id="YP<?php echo $scoutID; ?>">
		<h5 class="mb-0" data-toggle="collapse" data-target="#collapse<?php echo $scoutID; ?>" aria-expanded="true" aria-controls="collapse<?php echo $scoutID; ?>">
			<?php echo $name; ?>
			<?php if ($show_rank_badge == 1) : ?>
				<span class="badge badge-rank"><?php echo $lodge_level; ?></span>
			<?php endif; ?>
			<?php if ($show_lodge_badge == 1) : ?>

				<span class="badge badge-group pull-right"><?php echo $lodge; ?></span>

			<?php endif; ?>
		</h5>
	</div>

	<div id="collapse<?php echo $scoutID; ?>" class="collapse" role="tabpanel" aria-labelledby="YP<?php echo $scoutID; ?>" data-parent="#badgeListAccordion">
		<div class="card-body">

			<?php fetch_badges_by_person ($scoutID, $badgeRecords, $badgetype, $showProgress, $showAwarded); ?>

		</div>
	</div>
</div>

