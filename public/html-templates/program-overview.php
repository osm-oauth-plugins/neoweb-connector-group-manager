
	<div class="meetingContainer card">

		<?php
		/*
		$allowCalDownload
		*/
		?>

		<div class="meetingIntro card-header" role="tab" id="heading<?php echo $night["eveningid"]; ?>">
			<div data-toggle="collapse" data-target="#collapse<?php echo $night["eveningid"]; ?>">
				<h3 class="card-title"><?php echo $night["title"]; ?></h3>
				<div class="row">
					<div class="col-md-4">
					<span class="meetingDate">
						Date:<br/>
						<i class="fa fa-calendar-alt"></i>
						<?php

                        //Our YYYY-MM-DD date.
                        //Convert it into a timestamp.
                        $timestamp = strtotime($night["meetingdate"]);

                        //Convert it to DD-MM-YYYY
                        $meetingDate = date("d-m-Y", $timestamp);

                        echo $meetingDate; ?>

					</span>
					</div>
					<div class="col-md-4">
					<span class="meetingStartTime">
						Start time:<br/>
						<i class="fa fa-clock"></i>
						<?php echo substr($night["starttime"], 0, -3); ?>
					</span>
					</div>
					<div class="col-md-4">
					<span class="meetingEndTime">
						End time:<br/>
						<i class="fa fa-clock"></i>
						<?php echo substr($night["endtime"], 0, -3); ?>
					</span>
					</div>
				</div>
			</div>
		</div>
		<div id="collapse<?php echo $night["eveningid"]; ?>" class="collapse" role="tabpanel" aria-labelledby="heading<?php echo $night["eveningid"]; ?>" data-parent="#accordionMeeting">
			<ul class="meetingInfo list-group list-group-flush">
				<?php if ($showParentNotes == 1) { ?>
					<li class="parentNotes list-group-item">
						<h3>Parent notes:</h3>
						<p><?php echo $night["notesforparents"]; ?></p>
					</li>
				<?php } ?>

				<?php if ($showParentHelpNotes == 1) { ?>
					<li class="parentHelperNotes list-group-item">
						<h3>Parent helper notes:</h3>
						<p><?php echo $night["notesforhelpingparents"]; ?></p>
					</li>
				<?php } ?>

				<?php if ($showParentHelperCount == 1) { ?>
					<li class="parentHelpersRequired list-group-item">
						<h3>Number parents required:</h3>
						<p><?php echo $night["parentsrequired"]; ?></p>
					</li>
				<?php } ?>

				<?php if ($showPreNotes == 1) { ?>
					<li class="preMeetingNotes list-group-item">
						<h3>Post Meeting Notes:</h3>
						<p><?php echo $night["prenotes"]; ?></p>
					</li>
				<?php } ?>

				<?php if ($showGameNotes == 1) { ?>
					<li class="gameNotes list-group-item">
						<h3>Meeting / Game Notes:</h3>
						<p><?php echo $night["games"]; ?></p>
					</li>
				<?php } ?>

				<?php if ($showPostNotes == 1) { ?>
					<li class="postMeetingNotes list-group-item">
						<h3>Post Meeting Notes:</h3>
						<p><?php echo $night["postnotes"]; ?></p>
					</li>
				<?php } ?>

				<?php if ($showLeaderNotes == 1) { ?>
					<li class="leaderNotes list-group-item">
						<h3>Leaders Notes:</h3>
						<p><?php echo $night["leaders"]; ?></p>
					</li>
				<?php } ?>
			</ul>
			<?php if ($showBadges == 1) { ?>
                <?php $this->fetch_osm_program_data( $sectionID, $currentTermID, $night['eveningid'] ); ?>
			<?php } ?>
		</div>
	</div>
